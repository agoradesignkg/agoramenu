<?php

namespace Drupal\agoramenu\Plugin\Block;

use Drupal\Core\Menu\Plugin\Block\LocalTasksBlock;

/**
 * Provides the local tasks block with a different theme.
 *
 * @Block(
 *   id = "local_tasks_menu_block",
 *   admin_label = @Translation("Tabs as menu"),
 *   category = "Agoramenu"
 * )
 */
class LocalTasksMenuBlock extends LocalTasksBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();
    if (!empty($build['#theme'])) {
      $build['#theme'] = 'menu_local_tasks_menu';
      foreach ($build['#primary'] as &$item) {
        $item['#theme'] = 'menu_local_task_menu_item';
      }
      foreach ($build['#secondary'] as &$item) {
        $item['#theme'] = 'menu_local_task_menu_item';
      }
    }
    return $build;
  }

}

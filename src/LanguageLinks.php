<?php

namespace Drupal\agoramenu;

use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Helper class for building language links.
 */
class LanguageLinks {

  const LANGUAGE_BLOCK_KEY = '[LANGUAGE_BLOCK]';

  /**
   * Checks if language links should be added to the given menu link item.
   *
   * Checks whether a menu item render array is applicable for getting a
   * language links sub menu added.
   *
   * @param array $link_item
   *   The menu item render array, as it is delivered by hook_preprocess_menu().
   *   The $link_item is a single value of the preprocessor's
   *   $variables['items'].
   *
   * @return bool
   *   Whether the language links should be added as children to the given menu
   *   link item or not.
   */
  public static function isApplicable(array $link_item) {
    return !empty($link_item['title']) && $link_item['title'] == self::LANGUAGE_BLOCK_KEY;
  }

  /**
   * Populates the given menu item render array with a language links sub menu.
   *
   * Populates the given menu item render array with a language links sub menu
   * and changes it's title to the current language.
   *
   * @param array $link_item
   *   The menu item render array, as it is delivered by hook_preprocess_menu().
   *   The $link_item is a single value of the preprocessor's
   *   $variables['items'].
   */
  public static function populateLinkItem(array &$link_item) {
    $language = \Drupal::languageManager();
    $current_language = $language->getCurrentLanguage();
    $link_item['title'] = $current_language->getName();
    $link_item['below'] = self::createLanguageLinks();
  }

  /**
   * Creates renderable language switch links.
   *
   * @return array
   *   Creates renderable language switch links.
   */
  public static function createLanguageLinks() {
    $language = \Drupal::languageManager();
    $path_matcher = \Drupal::service('path.matcher');
    $route_name = $path_matcher->isFrontPage() ? '<front>' : '<current>';
    $links = $language->getLanguageSwitchLinks('language_interface', Url::fromRoute($route_name));
    if (!empty($links->links)) {
      foreach ($links->links as $link) {
        /** @var \Drupal\Core\Url $link['url'] */
        $link['url']->setOption('language', $link['language']);
      }
    }
    $language_links = [];
    if (!empty($links->links)) {
      $language_links = $links->links;
      foreach ($language_links as &$language_link) {
        if (!empty($language_link['attributes']) && !($language_link['attributes'] instanceof Attribute)) {
          $language_link['attributes'] = new Attribute($language_link['attributes']);
        }
      }
    }
    return $language_links;
  }

}
